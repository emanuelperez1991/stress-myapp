package main

import (
	"flag"
	"fmt"
	"ignite91/stress-myapp/grpc"
	"ignite91/stress-myapp/rest"
	"ignite91/stress-myapp/result"
	"time"
)

func main() {
	calls := flag.Int("calls", 0, "times of calls.")
	goRoutine := flag.Bool("go", false, "if is concurrent the calls.")
	r := flag.Bool("rest", false, "rest method.")
	g := flag.Bool("grpc", false, "grpc method.")
	flag.Parse()

	t := time.Now()

	if *r {
		switch {
		case *calls > 0:
			restReq := rest.RestRequest{
				Method: "GET",
				Url:    "http://localhost:",
				Port:   "8080",
				Path:   "/test"}
			if *goRoutine {
				restReq.DoConcurrent(calls)
			} else {
				restReq.DoSequential(calls)
			}
		default:
			fmt.Println("invalid parameter.")
		}
	}
	if *g {
		switch {
		case *calls > 0:
			var grpcReq *grpc.GrpcRequest
			if *goRoutine {
				grpcReq.DoConcurrent(calls)
			} else {
				grpcReq.DoSequential(calls)
			}
		default:
			fmt.Println("invalid parameter.")
		}
	}

	res := result.PrintResult(calls, goRoutine, t)
	err := result.SaveResult(res)
	if err != nil {
		panic(err)
	}
}
