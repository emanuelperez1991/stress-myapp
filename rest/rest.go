package rest

import (
	"log"
	"net/http"
	"sync"
)

type RestRequest struct {
	Url    string `json:"url"`
	Port   string `json:"port"`
	Path   string `json:"path"`
	Method string `json:"method"`
}

func (restReq *RestRequest) DoSequential(calls *int) {
	for i := 0; i < *calls; i++ {
		err := restReq.DoRequest()
		if err != nil {
			log.Fatal(err)
		}
	}
}

func (restReq *RestRequest) DoConcurrent(calls *int) {
	var wg sync.WaitGroup
	wg.Add(*calls)

	for i := 0; i < *calls; i++ {
		go func() {
			defer wg.Done()
			err := restReq.DoRequest()
			if err != nil {
				log.Fatal(err)
			}
		}()
	}
	wg.Wait()
}

func (restReq *RestRequest) DoRequest() error {
	req, err := http.NewRequest(restReq.Method, restReq.Url+restReq.Port+restReq.Path, nil)
	if err != nil {
		return err
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	return nil
}
