package grpc

import (
	"context"
	"log"
	"sync"
	"time"

	"ignite91/stress-myapp/pubsub/pubsubpb"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type GrpcRequest struct {
	Url    string `json:"url"`
	Port   string `json:"port"`
	Path   string `json:"path"`
	Method string `json:"method"`
}

func (grpcRequest *GrpcRequest) DoSequential(calls *int) {
	c := grpcRequest.GetConnection()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)

	defer cancel()

	for i := 0; i < *calls; i++ {
		_, err := c.Publish(ctx, &pubsubpb.PublishRequest{})
		if err != nil {
			log.Fatal(err)
		}
	}
}

func (grpcRequest *GrpcRequest) DoConcurrent(calls *int) {
	c := grpcRequest.GetConnection()
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)

	defer cancel()

	var wg sync.WaitGroup
	wg.Add(*calls)

	for i := 0; i < *calls; i++ {
		go func() {
			defer wg.Done()
			_, err := c.Publish(ctx, &pubsubpb.PublishRequest{})
			if err != nil {
				log.Fatal(err)
			}
		}()
	}
	wg.Wait()
}

func (grpcRequest *GrpcRequest) GetConnection() pubsubpb.PublisherClient {
	conn, err := grpc.Dial("localhost:50051", grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}

	c := pubsubpb.NewPublisherClient(conn)

	return c
}
